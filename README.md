# Emacs installation
  
See [Documentation](https://www.gnu.org/software/emacs/download.html)

## Windows 

Make sure to set emacs home using the following [documentation](https://www.gnu.org/software/emacs/manual/html_node/emacs/Windows-HOME.html)

# Doom Emacs

Use this [getting started](https://github.com/doomemacs/doomemacs/blob/master/docs/getting_started.org)

# Clone this configuration

Clone this repository in the doom config folder.

For example:

``` shell
rm -rf ~/.config/doom
git clone https://framagit.org/adelpeyroux/doom-config.git ~/.config/doom
```

Then just execute doom sync:

``` shell
doom sync
```

# Dependencies

This config will need some dependencies. 

## C/C++

* ccls
* cmake 
* clang or gcc
* bear

## haskell

* ghcup 

## clojure

* clojure
* java
* leiningen
* clj-kondo
* clojure-lsp
